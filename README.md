# README #

El acceso a la aplicación es el siguiente: http://127.0.0.1:8000

La base de datos que se uso fue en MySQL.

Al ingresar la primera GUI muestra la opción para conectarse a un sesión de juego o crearla.

Ejecutar el proyecto desde la carpeta AppTicTacToe.


# Alcance proyecto #

- Crear la sesión juego con un código para compartir a otro jugador

- guardar los nombres de los jugadores y mostrarlos en pantalla.

- Acceder con el código al juego un solo jugador 2.

- Que solo se pueda marcar una sola vez la casilla.

- Que logre ver en dos pantalla la seleccion de cada casilla TicTacToe.

- Finalizar el Juego.

- Se realizo el tema seeders y migrate

# No alcance del proyecto #

- Generar un nuevo turno y que se actualice en los dos pantallas

- Cada vez que se reinice el juego inicie con  jugador diferente.

- Identificar el ganador por el nombre.

- No se maneja sesiones o login.