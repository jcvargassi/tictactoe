<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SesionesController;
use App\Http\Controllers\JuegosController;
use App\Http\Controllers\MarcacionController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('juegos.Ingresojuego');
});

Route::get('/create', function () {
    return view('juegos.sesiones');
});

Route::post('/', [SesionesController::class, 'store']);
Route::post('/juego', [SesionesController::class, 'update']);
Route::post('/juegosEditar', [JuegosController::class, 'update']);
Route::get('/update/{id}', [MarcacionController::class, 'update']);
Route::post('/show/{id}', [SesionesController::class, 'show']);
Route::get('/newJuego/{id}', [MarcacionController::class, 'store']);
Route::post('/mostrar', [MarcacionController::class, 'edit']);

/*Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');*/
