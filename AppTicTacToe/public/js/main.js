


let _token = document.getElementById('token');
let id_juego = document.getElementById('id_juego');
// Funcion para definir la marcación de la X o O
function addOpcion(cuadro) {
    if (document.getElementById(`marcar_${cuadro}`).innerHTML === 'X' || document.getElementById(`marcar_${cuadro}`).innerHTML === 'O') {
        return;
    }

    let obj = { idjuego: id_juego.value, pos: cuadro };
    fetch('http://127.0.0.1:8000/juegosEditar', {
        method: 'POST',
        mode: 'cors',
        headers: {
            'X-CSRF-TOKEN': _token.value,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
    })
        .then(res => res.json())
        .then(res => {
            if(res['datos'] == 0){
                window.open('http://127.0.0.1:8000/', '_self');
                clearInterval(id);
            }
            switch (cuadro) {
                case 1:
                    document.getElementById('marcar_1').innerHTML = res.marca;
                    break;
                case 2:
                    document.getElementById('marcar_2').innerHTML = res.marca;
                    break;
                case 3:
                    document.getElementById('marcar_3').innerHTML = res.marca;
                    break;
                case 4:
                    document.getElementById('marcar_4').innerHTML = res.marca;
                    break;
                case 5:
                    document.getElementById('marcar_5').innerHTML = res.marca;
                    break;
                case 6:
                    document.getElementById('marcar_6').innerHTML = res.marca;
                    break;
                case 7:
                    document.getElementById('marcar_7').innerHTML = res.marca;
                    break;
                case 8:
                    document.getElementById('marcar_8').innerHTML = res.marca;
                    break;
                default:
                    document.getElementById('marcar_9').innerHTML = res.marca;
                    break;
            }

            if (res.ganador != '0' && res.ganador != 'Nadie gano') {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: res.ganador,
                    showConfirmButton: false,
                    timer: 1500
                })
                document.getElementById('newRonda').style.display = 'block';
            } else if (res.ganador == 'Nadie gano') {
                Swal.fire({
                    position: 'center',
                    icon: 'warning',
                    title: res.ganador,
                    showConfirmButton: false,
                    timer: 1500
                })
                document.getElementById('newRonda').style.display = 'block'
            }

        })
        .catch(function (error) {
            console.log(error)
        });


}
// Función para mostrar al otro jugador lo que se marcado
if (!!document.getElementById('id_juego')) {
const id =  setInterval(() => {
        let _token = document.getElementById('token');
        let id_juego = document.getElementById('id_juego');
        let obj = { idjuego: id_juego.value};
        fetch('http://127.0.0.1:8000/mostrar', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'X-CSRF-TOKEN': _token.value,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        })
            .then(res => res.json())
            .then(res => {
                if(res['datos'] == 0){
                    windows.open('http://127.0.0.1:8000/', '_self');
                    clearInterval(id);
                }
                if(res[0]['marcacion_1'] !== null){
                    document.getElementById('marcar_1').innerHTML = res[0]['marcacion_1'];
                }
                if(res[0]['marcacion_2'] !== null){
                    document.getElementById('marcar_2').innerHTML = res[0]['marcacion_2'];
                }
                if(res[0]['marcacion_3'] !== null){
                    document.getElementById('marcar_3').innerHTML = res[0]['marcacion_3'];
                }
                if(res[0]['marcacion_4'] !== null){
                    document.getElementById('marcar_4').innerHTML = res[0]['marcacion_4'];
                }
                if(res[0]['marcacion_5'] !== null){
                    document.getElementById('marcar_5').innerHTML = res[0]['marcacion_5'];
                }
                if(res[0]['marcacion_6'] !== null){
                    document.getElementById('marcar_6').innerHTML = res[0]['marcacion_6'];
                }
                if(res[0]['marcacion_7'] !== null){
                    document.getElementById('marcar_7').innerHTML = res[0]['marcacion_7'];
                }
                if(res[0]['marcacion_8'] !== null){
                    document.getElementById('marcar_8').innerHTML = res[0]['marcacion_8'];
                }
                if(res[0]['marcacion_9'] !== null){
                    document.getElementById('marcar_9').innerHTML = res[0]['marcacion_9'];
                }
               
                if (res[1]['ganador'] != '0' && res[1]['ganador'] != 'Nadie gano') {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: res[1]['ganador'],
                        showConfirmButton: false,
                        timer: 1500
                    })
                    document.getElementById('newRonda').style.display = 'block';
                    clearInterval(id);
                } else if (res[1]['ganador'] == 'Nadie gano') {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: res[1]['ganador'],
                        showConfirmButton: false,
                        timer: 1500
                    })
                    document.getElementById('newRonda').style.display = 'block'
                    clearInterval(id);
                }

            })
            .catch(function (error) {
                console.log(error)
            });


    }, 1000);
}


