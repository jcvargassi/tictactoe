@extends('layouts.app')
@section('content')
<div class="container">
@if(Session::has('mensaje'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>Atención</strong> {{Session::get('mensaje')}}.
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
    <form action="{{url('/juego')}}" method="post" autocomplete="off">
        @csrf
        <div class="row" style="display: flex; justify-content: center;">
            <div class="form-group col-md-4">
                <label class="form-label">Ingreso</label>
                <input class="form-control" placeholder="Ingrese PIN" type="tel" name="pinJuego" id="pinJuego">
            </div>
            <div class="form-group col-md-4">
                <label for="" class="form-label">Nombre Jugador</label>
                <input class="form-control" type="tel" name="nombrejug2" id="nombrejug2" required>
            </div>
        </div>
        <br>
        <div class="row" style="display: flex; justify-content: center;">
            <div class="form-group col-md-6">
                <input class="btn btn-info" type="submit" value="Iniciar juego">
                <a class="btn btn-primary" href="{{ url('/create/');}}">Crear Juego</a>
            </div>
            
        </div>
    </form>
</div>
@endsection