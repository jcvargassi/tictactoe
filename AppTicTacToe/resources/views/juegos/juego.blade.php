@extends('layouts.app')
@section('content')
<div class="container">
    <p class="text-center"><b>Jugador 1:</b> {{$sesiones[0]['nombrejug1']}} <b class="ms-2">Jugador 2:</b> {{$sesiones[0]['nombrejug2']}}</p>
    <div class="container" style="display: flex; justify-content: center;">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" />
        <input type="hidden" name="id_juego" id="id_juego" value="{{ $sesiones[1]['id_juego'] }}" />
        <div class="row row-cols-3 row-cols-lg-3 g-2 g-lg-3 col-md-6">
            <div class="col-md-2">
                <div class="p-3 border bg-light btn" onclick="addOpcion(1);" id="marcar_1" style="width: 100%">Seleccione
                </div>
            </div>
            <div class="col-md-2">
                <div class="p-3 border bg-light btn " onclick="addOpcion(2);"  id="marcar_2" style="width: 100%">Seleccione</div>
            </div>
            <div class="col-md-2">
                <div class="p-3 border bg-light btn" onclick="addOpcion(3);"  id="marcar_3" style="width: 100%">Seleccione</div>
            </div>
            <div class="col-md-2">
                <div class="p-3 border bg-light btn" onclick="addOpcion(4);" id="marcar_4" style="width: 100%">Seleccione</div>
            </div>
            <div class="col-md-2">
                <div class="p-3 border bg-light btn" onclick="addOpcion(5);" id="marcar_5" style="width: 100%">Seleccione</div>
            </div>
            <div class="col-md-2">
                <div class="p-3 border bg-light btn" onclick="addOpcion(6);" id="marcar_6" style="width: 100%">Seleccione</div>
            </div>
            <div class="col-md-2">
                <div class="p-3 border bg-light btn" onclick="addOpcion(7);" id="marcar_7" style="width: 100%">Seleccione</div>
            </div>
            <div class="col-md-2">
                <div class="p-3 border bg-light btn" onclick="addOpcion(8);" id="marcar_8" style="width: 100%">Seleccione</div>
            </div>
            <div class="col-md-2">
                <div class="p-3 border bg-light btn" onclick="addOpcion(9);"  id="marcar_9" style="width: 100%">Seleccione</div>
            </div>
        </div>
    </div>
    <br>
    <div class="row" >
        <div class="col" style="display: flex; justify-content: center;">
            <a class="btn btn-outline-primary" href="{{ url('/update/'.$sesiones[0]['id'])}}" role="button">Finalizar Juego</a>
            <a class="btn btn-outline-info ms-2" href="{{ url('/newJuego/'.$sesiones[0]['urlsesionjuego']);}}" role="button" style="display: none;" id="newRonda" >Nuevo Ronda</a>
        </div>
    </div>
</div>
</div>

@endsection