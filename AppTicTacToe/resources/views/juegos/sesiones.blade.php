@extends('layouts.app')
@section('content')
<div class="container">
    @if(Session::has('code') || Session::has('mensaje'))
    <form action="{{ url('/show/'.Session::get('code'));}}" method="post" autocomplete="off">
    @csrf
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Empieza La diversión: </strong> <h3>Comparte este Código para jugar con otro Amigo: <span class="badge bg-info"> {{Session::get('code')}} </span></h3>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @if(Session::has('mensaje'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <span>{{Session::get('mensaje')}}</span>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
        <br>
        <div class="form-group col-md-6">
            <input class="btn btn-info" type="submit" value="Iniciar juego">
        </div>
    </form>
    @else
    <strong class="h3" style="display: flex; justify-content: center;">Crear Juego</strong>
    <form action="{{ url('/') }}" method="post" autocomplete="off">
        @csrf
        <div class="row" style="display: flex; justify-content: center;">
            <div class="form-group col-md-6">
                <label for="" class="form-label">Nombre Jugador</label>
                <input class="form-control" type="tel" name="nombrejug1" id="nombrejug1" required>
            </div>
        </div>
        <br>
        <div class="row" style="display: flex; justify-content: center;">
            <div class="form-group col-md-2">
                <input class="btn btn-primary" type="submit" value="Crear juego">
            </div>
        </div>
    </form>
    @endif



</div>
@endsection