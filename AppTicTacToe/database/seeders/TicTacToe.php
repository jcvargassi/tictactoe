<?php

namespace Database\Seeders;
use App\Models\Juegos;
use App\Models\marcacion;
use App\Models\Sesiones;

use Illuminate\Database\Seeder;

class TicTacToe extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // Esta semilla es solo para que se cree la sesion para el jugador 1. el jugador 2 debe hacer por web. Aunque esto es un ejempo de registro de 
        // Semilla pero la funcionalidad TicTacToe es por la web
        // Crear Sesiones
        $sesiones = new Sesiones();
        $sesiones->nombrejug1 = 'Juan';
        $sesiones->urlsesionjuego = 'Prueba352352362';
        $sesiones->estado = '0';
        $sesiones->save();

        //Crear juegos
        $juegos = new Juegos();
        $juegos->id_sesion = $sesiones['id'];
        $juegos->turno = '1';
        $juegos->estadojuego = '0';
        $juegos->save();

        //Crear Marcaciones
        $marcaciones = new marcacion();
        $marcaciones->id_juego = $juegos['id'];
        $marcaciones->save();

    }
}
