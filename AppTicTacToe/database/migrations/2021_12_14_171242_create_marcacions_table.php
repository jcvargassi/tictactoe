<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarcacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marcacions', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->bigIncrements('id_marcacion');
            $table->bigInteger('id_juego')->unsigned();;
            $table->char('marcacion', 2)->nullable();
            $table->char('marcacion_1', 2)->nullable();
            $table->char('marcacion_2', 2)->nullable();
            $table->char('marcacion_3', 2)->nullable();
            $table->char('marcacion_4', 2)->nullable();
            $table->char('marcacion_5', 2)->nullable();
            $table->char('marcacion_6', 2)->nullable();
            $table->char('marcacion_7', 2)->nullable();
            $table->char('marcacion_8', 2)->nullable();
            $table->char('marcacion_9', 2)->nullable();
            $table->timestamps();
            $table->foreign('id_juego')->references('id_juego')->on('juegos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marcacions');
    }
}
