<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJuegosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('juegos', function (Blueprint $table) {
            $table->engine="InnoDB";
            $table->bigIncrements('id_juego');
            $table->bigInteger('id_sesion')->unsigned();
            $table->string('turno')->nullable();
            $table->string('estadojuego')->nullable();
            $table->timestamps();
            $table->foreign('id_sesion')->references('id')->on('sesiones')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('juegos');
    }
}
