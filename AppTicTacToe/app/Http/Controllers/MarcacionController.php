<?php

namespace App\Http\Controllers;

use App\Models\marcacion;
use App\Models\Juegos;
use App\Models\Sesiones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MarcacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        //Lógica para volver a jugar un turno
        // Se realiza la consulta para saber si esta activo el juego
        $sesiones = sesiones::where('urlsesionjuego', $id)->where('estado', '0')->get();
        if (count($sesiones) > 0) {
            $ids = ($sesiones[0]['id']);
            $juegos = Juegos::where('id_sesion', $ids)->get();
            if ($juegos[0]['turno'] === '1') {
                $turno = '2';
            } else {
                $turno = '1';
            }

            // Se realiza un update para el turno 
            Juegos::where('id_juego', '=', $juegos[0]['id_juego'])->update(['turno' => $turno]);

            // Se inserta las diferente marcaciones y sesiones de juego
            $marcacion = new marcacion();
            $marcacion->id_juego = $juegos[0]['id_juego'];
            $marcacion->save();

            $sesiones[] = array(
                'id_juego' => $juegos[0]['id_juego']
            );

            return view('juegos.juego', compact('sesiones'));
        } else {
            return redirect('/')->with('mensaje', 'El juego no esta disponible. ');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\marcacion  $marcacion
     * @return \Illuminate\Http\Response
     */
    public function show(marcacion $marcacion)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\marcacion  $marcacion
     * @return \Illuminate\Http\Response
     */
    public function edit(marcacion $marcacion, Request $request)
    {
        //
        $marcacion = $request->input('idjuego');
        $ganador = '0';
        $juegos = DB::table('marcacions')
            ->join('juegos', 'juegos.id_juego', '=', 'marcacions.id_juego')
            ->select('turno', 'marcacion', 'estadojuego', 'marcacion_1', 'marcacion_2', 'marcacion_3', 'marcacion_4', 'marcacion_5', 'marcacion_6', 'marcacion_7', 'marcacion_8', 'marcacion_9')
            ->where('marcacions.id_juego', '=', $marcacion)
            ->where('estadojuego', '=', '0')
            ->orderBy('id_marcacion', 'DESC')
            ->get();

        if (count($juegos) > 0) {
            // De manera vertical
            if ($juegos[0]->marcacion_1 === 'X' && $juegos[0]->marcacion_2 === 'X' && $juegos[0]->marcacion_3 === 'X') {
                $ganador = 'Gano X';
            } else if ($juegos[0]->marcacion_4 ===  'X' && $juegos[0]->marcacion_5 === 'X' && $juegos[0]->marcacion_6 === 'X') {
                $ganador = 'Gano X';
            } else if ($juegos[0]->marcacion_7 === 'X' && $juegos[0]->marcacion_8 === 'X' && $juegos[0]->marcacion_9 === 'X') {
                $ganador = 'Gano X';
            }
            // De manera Horizontal
            else if ($juegos[0]->marcacion_1 === 'X' && $juegos[0]->marcacion_4 === 'X' && $juegos[0]->marcacion_7 === 'X') {
                $ganador = 'Gano X';
            } else if ($juegos[0]->marcacion_2 === 'X' && $juegos[0]->marcacion_5 === 'X' &&  $juegos[0]->marcacion_8 === 'X') {
                $ganador = 'Gano X';
            } else if ($juegos[0]->marcacion_3 === 'X' && $juegos[0]->marcacion_6 === 'X' &&  $juegos[0]->marcacion_9 === 'X') {
                $ganador = 'Gano X';
            }

            // De manera Diagonal
            else if ($juegos[0]->marcacion_1 === 'X' && $juegos[0]->marcacion_5 === 'X' &&  $juegos[0]->marcacion_9 === 'X') {
                $ganador = 'Gano X';
            } else if ($juegos[0]->marcacion_7 ===  'X' && $juegos[0]->marcacion_5 === 'X'  && $juegos[0]->marcacion_3 === 'X') {
                $ganador = 'Gano X';
            }

            // De manera vertical
            else if ($juegos[0]->marcacion_1 === 'O' && $juegos[0]->marcacion_2 === 'O' && $juegos[0]->marcacion_3 === 'O') {
                $ganador = 'Gano O';
            } else if ($juegos[0]->marcacion_4 ===  'O' && $juegos[0]->marcacion_5 === 'O' && $juegos[0]->marcacion_6 === 'O') {
                $ganador = 'Gano O';
            } else if ($juegos[0]->marcacion_7 === 'O' && $juegos[0]->marcacion_8 === 'O' && $juegos[0]->marcacion_9 === 'O') {
                $ganador = 'Gano O';
            }
            // De manera Horizontal
            else if ($juegos[0]->marcacion_1 === 'O' && $juegos[0]->marcacion_4 === 'O' && $juegos[0]->marcacion_7 === 'O') {
                $ganador = 'Gano O';
            } else if ($juegos[0]->marcacion_2 === 'O' && $juegos[0]->marcacion_5 === 'O' &&  $juegos[0]->marcacion_8 === 'O') {
                $ganador = 'Gano O';
            } else if ($juegos[0]->marcacion_3 === 'O' && $juegos[0]->marcacion_6 === 'O' &&  $juegos[0]->marcacion_9 === 'O') {
                $ganador = 'Gano O';
            }

            // De manera Diagonal
            else if ($juegos[0]->marcacion_1 === 'O' && $juegos[0]->marcacion_5 === 'O' &&  $juegos[0]->marcacion_9 === 'O') {
                $ganador = 'Gano O';
            } else if ($juegos[0]->marcacion_7 ===  'O' && $juegos[0]->marcacion_5 === 'O'  && $juegos[0]->marcacion_3 === 'O') {
                $ganador = 'Gano O';
            } else if (
                $juegos[0]->marcacion_1 !== null && $juegos[0]->marcacion_2 !== null  && $juegos[0]->marcacion_3 !== null &&
                $juegos[0]->marcacion_4 !== null  && $juegos[0]->marcacion_5 !== null  && $juegos[0]->marcacion_6 !== null &&
                $juegos[0]->marcacion_7 !== null && $juegos[0]->marcacion_8 !== null && $juegos[0]->marcacion_9 !== null
            ) {
                $ganador = 'Nadie gano';
            }

            $juegos[] = array(
                'turno' => $juegos[0]->turno,
                'ganador' => $ganador
            );

            return response()->json($juegos);
        }else{
            $array = array(
                'datos' => 0
            );
            return response()->json($array);
           //return redirect('/')->with('mensaje','El juego no esta disponible');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\marcacion  $marcacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {

        sesiones::where('id', '=', $id)->update(['estado' => '1']);
        Juegos::where('id_sesion', '=', $id)->update(['estadojuego' => '1']);
        return view('juegos.Ingresojuego');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\marcacion  $marcacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(marcacion $marcacion)
    {
        //
    }
}
