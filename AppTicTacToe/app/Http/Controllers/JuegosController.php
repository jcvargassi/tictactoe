<?php

namespace App\Http\Controllers;

use App\Models\Juegos;
use App\Models\marcacion;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Redirect;


class JuegosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Juegos  $juegos
     * @return \Illuminate\Http\Response
     */
    public function show(Juegos $juegos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Juegos  $juegos
     * @return \Illuminate\Http\Response
     */
    public function edit(Juegos $juegos)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Juegos  $juegos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Juegos $juegos)
    {
        //Se recibe las variables
        $idjuego = $request->input('idjuego');
        $pos = $request->input('pos');
        $ganador = '0';
        //Se realiza la consulta de los datos almacenados de marcacion y el juego
        $juegos = DB::table('marcacions')
            ->join('juegos', 'juegos.id_juego', '=', 'marcacions.id_juego')
            ->select('turno', 'marcacion', 'estadojuego', 'marcacion_1', 'marcacion_2', 'marcacion_3', 'marcacion_4', 'marcacion_5', 'marcacion_6', 'marcacion_7', 'marcacion_8', 'marcacion_9')
            ->where('marcacions.id_juego', '=', $idjuego)
            ->where('estadojuego', '=', '0')
            ->orderBy('id_marcacion', 'DESC')
            ->get();
        // Se valida cual fue la última marcación
        if(count($juegos) > 0){
            switch ($juegos[0]->marcacion) {
                case '':
                    $marca = 'X';
                    break;
                case 'X':
                    $marca = 'O';
                    break;
                default:
                    $marca = 'X';
                    break;
            }
            // campo para ergistrar la seleccion de X o O
            $campo = "marcacion_$pos";
            $juegos[0]->$campo = $marca;
    
            // De manera vertical
            if ($juegos[0]->marcacion_1 === 'X' && $juegos[0]->marcacion_2 === 'X' && $juegos[0]->marcacion_3 === 'X') {
                $ganador = 'Gano X';
            } else if ($juegos[0]->marcacion_4 ===  'X' && $juegos[0]->marcacion_5 === 'X' && $juegos[0]->marcacion_6 === 'X') {
                $ganador = 'Gano X';
            } else if ($juegos[0]->marcacion_7 === 'X' && $juegos[0]->marcacion_8 === 'X' && $juegos[0]->marcacion_9 === 'X') {
                $ganador = 'Gano X';
            }
            // De manera Horizontal
            else if ($juegos[0]->marcacion_1 === 'X' && $juegos[0]->marcacion_4 === 'X' && $juegos[0]->marcacion_7 === 'X') {
                $ganador = 'Gano X';
            } else if ($juegos[0]->marcacion_2 === 'X' && $juegos[0]->marcacion_5 === 'X' &&  $juegos[0]->marcacion_8 === 'X') {
                $ganador = 'Gano X';
            } else if ($juegos[0]->marcacion_3 === 'X' && $juegos[0]->marcacion_6 === 'X' &&  $juegos[0]->marcacion_9 === 'X') {
                $ganador = 'Gano X';
            }
    
            // De manera Diagonal
            else if ($juegos[0]->marcacion_1 === 'X' && $juegos[0]->marcacion_5 === 'X' &&  $juegos[0]->marcacion_9 === 'X') {
                $ganador = 'Gano X';
            } else if ($juegos[0]->marcacion_7 ===  'X' && $juegos[0]->marcacion_5 === 'X'  && $juegos[0]->marcacion_3 === 'X') {
                $ganador = 'Gano X';
            }
    
            // De manera vertical
            else if ($juegos[0]->marcacion_1 === 'O' && $juegos[0]->marcacion_2 === 'O' && $juegos[0]->marcacion_3 === 'O') {
                $ganador = 'Gano O';
            } else if ($juegos[0]->marcacion_4 ===  'O' && $juegos[0]->marcacion_5 === 'O' && $juegos[0]->marcacion_6 === 'O') {
                $ganador = 'Gano O';
            } else if ($juegos[0]->marcacion_7 === 'O' && $juegos[0]->marcacion_8 === 'O' && $juegos[0]->marcacion_9 === 'O') {
                $ganador = 'Gano O';
            }
            // De manera Horizontal
            else if ($juegos[0]->marcacion_1 === 'O' && $juegos[0]->marcacion_4 === 'O' && $juegos[0]->marcacion_7 === 'O') {
                $ganador = 'Gano O';
            } else if ($juegos[0]->marcacion_2 === 'O' && $juegos[0]->marcacion_5 === 'O' &&  $juegos[0]->marcacion_8 === 'O') {
                $ganador = 'Gano O';
            } else if ($juegos[0]->marcacion_3 === 'O' && $juegos[0]->marcacion_6 === 'O' &&  $juegos[0]->marcacion_9 === 'O') {
                $ganador = 'Gano O';
            }
    
            // De manera Diagonal
            else if ($juegos[0]->marcacion_1 === 'O' && $juegos[0]->marcacion_5 === 'O' &&  $juegos[0]->marcacion_9 === 'O') {
                $ganador = 'Gano O';
            } else if ($juegos[0]->marcacion_7 ===  'O' && $juegos[0]->marcacion_5 === 'O'  && $juegos[0]->marcacion_3 === 'O') {
                $ganador = 'Gano O';
            }else if($juegos[0]->marcacion_1 !== null && $juegos[0]->marcacion_2 !== null  && $juegos[0]->marcacion_3 !== null &&
             $juegos[0]->marcacion_4 !== null  && $juegos[0]->marcacion_5 !== null  && $juegos[0]->marcacion_6 !== null && 
             $juegos[0]->marcacion_7 !== null && $juegos[0]->marcacion_8 !== null && $juegos[0]->marcacion_9 !== null){
                $ganador = 'Nadie gano';
            }
    
            // Se realiza le update de la selección de la marcación
    
            marcacion::where('id_juego', '=', $idjuego)->update(['marcacion' => $marca, $campo => $marca]);
            // Se retorna los resultado marcados
            $array = array(
                'turno' => $juegos[0]->turno,
                'marca' => $marca,
                'ganador' => $ganador
            );
           
            return response()->json($array);
        }else{
            $array = array(
                'datos' => 0
            );
            return response()->json($array);
            //return redirect('/')->with('mensaje','El juego no esta disponible');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Juegos  $juegos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Juegos $juegos)
    {
        //
    }
}
