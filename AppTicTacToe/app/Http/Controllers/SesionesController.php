<?php

namespace App\Http\Controllers;

use App\Models\Sesiones;
use App\Models\Juegos;
use App\Models\marcacion;
use Illuminate\Http\Request;

class SesionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //pagina inicial para iniciar juego o crearlo
        return view('juegos.ingresojuego');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Creacion de juego
        return view('juegos.sesiones');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Se crear la sesión y toKen para ingresar al juego
        $insert = new sesiones();
        $pin =  substr(sha1(time()), 0, 16);
        $datosJuego = request()->except('_token');
        $insert->nombrejug1 = $datosJuego['nombrejug1'];
        $insert->urlsesionjuego = $pin;
        $insert->estado = '0';
        $insert->save();  
        return redirect('/create')->with('code', $pin);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sesiones  $sesiones
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Se realiza una validación si el jugador puede ya ingresar a la jugar si existe el jugador 2
        $sesiones = sesiones::where('urlsesionjuego', $id)->where('estado', '0')->where('nombrejug2', '!=', 'null')->get();
        if(count($sesiones) > 0){
            $juego = Juegos::where('id_sesion', $sesiones[0]["id"])
                    ->where('estadojuego', '=', '0')
                    ->orderBy('id_juego', 'DESC')
                    ->get();
            $sesiones[] = array(
                'id_juego' => $juego[0]["id_juego"]
            );
            return view('juegos.juego', compact('sesiones'));
        }else{
            return redirect('/create')->with('mensaje', 'No se puede iniciar el Juego porque no hay Jugador 2.')->with('code', $id);
        }
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sesiones  $sesiones
     * @return \Illuminate\Http\Response
     */
    public function edit(Sesiones $sesiones)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sesiones  $sesiones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sesiones $sesiones)
    {
        //Se reciben las variables
        $datos = request()->except('_token');
        // Se consulta la sesion par saber que ya no exista otra jugador 2 y este activo
        $sesiones = sesiones::where('urlsesionjuego', $datos['pinJuego'])->where('estado', '0')->where('nombrejug2', null)->get();
        if(count($sesiones) > 0){
            // Se realiza un update para crera el nombre del jugador 2 en sesiones
            sesiones::where('urlsesionjuego','=',$datos['pinJuego'])->update(['nombrejug2' => $datos['nombrejug2']]);
            $id = ($sesiones[0]['id']);
            $sesiones[0]['nombrejug2'] = $datos['nombrejug2'];
            $turno = '1';
            // Se inserta el juego 
            $insert = new juegos();
            $insert->id_sesion = $id;
            $insert->turno = $turno;
            $insert->estadojuego = '0';
            $insert->save();
    
            // Se inserta las diferente marcaciones y sesiones de juego
            $marcacion = new marcacion();
            $marcacion->id_juego = $insert['id'];
            $marcacion->save();
    
            $sesiones[] = array(
                'id_juego' => $insert['id']
            );
    
            return view('juegos.juego', compact('sesiones'));
        }else{
            return redirect('/')->with('mensaje','El juego ya tiene todos sus jugadores');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sesiones  $sesiones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sesiones $sesiones)
    {
        //
    }
}
